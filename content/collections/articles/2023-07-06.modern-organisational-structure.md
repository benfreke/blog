---
id: 12
published: false
blueprint: article
title: 'My thoughts on a modern organisational structure for software development'
excerpt: 'A purely opinion piece on titles within a software development team.'
author:
  - 590daa05-4a9d-4d28-9463-75e492547549
updated_by: 590daa05-4a9d-4d28-9463-75e492547549
updated_at: 1695387359
table_of_contents: |-
  <ul class="table-of-contents">
  <li class="">
  <p><a href="#team-size" title="Team size">Team size</a></p>
  </li>
  </ul>
---
## Team size

It's been around a while, but a team shouldn't be larger than what you can feed with 2 pizzas.
Scrum would say 6 +- 3 so that does fit, depending on how much pizza each person eats!

## Roles within a team

Not all teams will have people with these roles, so these are

Developers at the bottom. Responsible for writing code, reviewing code, owning the architecture
Team Leads have direct reports, but still write code. Help product with the breakdown. Teams can be cross functional or not from a reporting point of view (but most likely not cross functional)
EM above them, ensure alignment in processes and architecture across teams.
Software Architect, ensuring the software gets built in the right way
Enterprise Architect, ensuring the connections in and out of the product are built correctly
Platform Developer, ensures the internal tooling is available for Teams to do what is needed

You will often see the Architect roles combined into a Principal Engineer role these days.

Observability is everyone's responsibility
Estimation is done by the people who do the work
Developers have multiple paths in which to grow
EM do not write production code

Many roles can be combined in smaller orgs

https://www.chieflearningofficer.com/2023/06/13/manager-mentor-or-coach-help-we-need-some-distinctions/?utm_source=brevo&utm_campaign=Level%20Up%20-%20Issue%20204&utm_medium=email
