#!/usr/bin/env bash
docker compose run --rm build php artisan statamic:stache:clear
docker compose run --rm build php artisan cache:clear
