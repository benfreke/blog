<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Statamic\Facades\Entry as EntryFacade;
use Statamic\Entries\Entry;

/**
 * Class Reparse
 *
 * Uses the built in Statamic tool set to re-parse files.
 * This allows the TOC markdown to be added without needing the control panel
 *
 * @package App\Console\Commands
 */
class Reparse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statamic:reparse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save all articles so TOC can work';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var Entry $item */
        try {
            foreach (EntryFacade::whereCollection('articles') as $item) {
                $this->info("$item->title saving");
                $item->save();
                $this->info("$item->title saved!");
                $this->info('');
            }
        } catch (Exception $exception) {
            if (!empty($item) && is_object($item)) {
                $this->error("Error with reparsing {$item->title}");
                $this->error("Message: {$exception->getMessage()}");
            }
            return 1;
        }
        return 0;
    }
}
