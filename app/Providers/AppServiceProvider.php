<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Njed\Toc\Extensions\CommonMark\TitleAnchorIdExtension;
use Statamic\Facades\Markdown;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Markdown::extend(
            'toc',
            function ($parser) {
                return $parser
                    ->withStatamicDefaults()
                    ->addExtension(
                        function () {
                            return new TitleAnchorIdExtension;
                        }
                    );
            }
        );
    }
}
